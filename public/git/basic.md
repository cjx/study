# Git常用命令

## 拉取代码

## git 如何恢复到指定版本

#### 1. 查看git的提交版本和id 拿到需要恢复的版本号 
>命令：git log
#### 2. 恢复到指定版本 
>命令：git reset --hard fabfafcbab6dbf1c9e79c7faaace1a1f7f959cf2

后面这一大串fabfafcbab6dbf1c9e79c7faaace1a1f7f959cf2就是版本id
#### 3. 强制push

>命令：git push -f origin master
<!-- end -->