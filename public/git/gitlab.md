# GitLab项目初始化
初始化代码库，或更改源。
## Git global setup
```
git config --global user.name "kxmgc"
git config --global user.email "kxmgc7@email.com"
```
## Create a new repository
```
git clone git@gitlab.com:labs27/cpp.git
cd cpp
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```
## Push an existing folder
```
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:labs27/cpp.git
git add .
git commit -m "Initial commit"
git push -u origin main
```
## Push an existing Git repository
```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:labs27/cpp.git
git push -u origin --all
git push -u origin --tags
```
