# Study notes 
https://cjx.gitlab.io/study/#/


https://study.kxmgc.com/#/

https://docsify.js.org/#/zh-cn/quickstart?id=%e5%bc%80%e5%a7%8b%e5%86%99%e6%96%87%e6%a1%a3

docsify serve docs

你需要在 ./docs 目录下创建一个 .nojekyll 文件，以防止 GitHub Pages 忽略下划线开头的文件。
.nojekyll 用于阻止 GitHub Pages 忽略掉下划线开头的文件

```
初始化成功后，可以看到 ./docs 目录下创建的几个文件

index.html 入口文件
README.md 会做为主页内容渲染
.nojekyll 用于阻止 GitHub Pages 忽略掉下划线开头的文件
```